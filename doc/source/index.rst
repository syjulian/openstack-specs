.. openstack-specs documentation master file, created by
   sphinx-quickstart on Wed May 25 00:37:25 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

============================
Community Team Specification
============================

Ocata Specifications
====================
.. toctree::
    :glob:
    :maxdepth: 1

    specs/ocata/index
